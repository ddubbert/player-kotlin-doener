package dev

import core.restadapter.GameInfoResponseDto
import game.domain.GameStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.lang.Thread.sleep

class GameAutoInitializer(private val gameAdminClient: GameAdminClient) {
    private val logger = LoggerFactory.getLogger(javaClass)

    companion object {
        private const val NUMBER_OF_ROUNDS = 10000
        private const val NUMBER_OF_PLAYERS = 1
        private const val ROUND_DURATION = 10000
    }

    suspend fun stopExistingGamesAndStartNewGame() {
        stopExistingGames()
        val newGameResponse = gameAdminClient.createGame(NUMBER_OF_ROUNDS, NUMBER_OF_PLAYERS)
        val newGameId = newGameResponse.gameId
        gameAdminClient.setRoundDuration(newGameId, ROUND_DURATION)
        startGameWhenPlayersAreConnected()
    }

    suspend fun stopExistingGames(){
        val games = gameAdminClient.getAllGames()

        for (game in games) {
            if (game.gameStatus == GameStatus.CREATED) {
                gameAdminClient.startGame(game.gameId)
                delay(2000) // Give the game some time to start, otherwise you might get duplicate games
            }
            gameAdminClient.stopGame(game.gameId)
            delay(1000)
        }
    }


    private suspend fun startGameWhenPlayersAreConnected() {
        CoroutineScope(Dispatchers.IO).launch {
            var game = createOrFindPendingGame()
            while (game.participatingPlayers.size < NUMBER_OF_PLAYERS) {
                val remainingPlayers = NUMBER_OF_PLAYERS - game.participatingPlayers.size
                logger.info("Waiting for $remainingPlayers more Players to connect! ${game.participatingPlayers.size}/$NUMBER_OF_PLAYERS players connected")
                sleep(5000)
                game = createOrFindPendingGame()
            }
            logger.info("${game.participatingPlayers.size} players connected. Starting game")
            gameAdminClient.startGame(game.gameId)
        }
    }

    private suspend fun createOrFindPendingGame(): GameInfoResponseDto {
        val pendingGame = gameAdminClient.getAllGames().firstOrNull { it.gameStatus == GameStatus.CREATED }
        return if(pendingGame != null) {
             pendingGame
        } else {
            val newGameResponse = gameAdminClient.createGame(NUMBER_OF_ROUNDS, NUMBER_OF_PLAYERS)
            gameAdminClient.setRoundDuration(newGameResponse.gameId, ROUND_DURATION)
            gameAdminClient.getGame(newGameResponse.gameId)!!
        }
    }


}