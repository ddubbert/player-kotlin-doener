package dev.dto

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import java.util.*
@Serializable
data class CreateGameResponseDto(
    @Serializable(with = UUIDSerializer::class)
    val gameId : UUID
)