package dev.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SetRoundDurationRequestDto(
    @SerialName("duration")
    private val durationInMs: Int
) {
    init {
        require(durationInMs > 0) { "durationInMs must be greater than 0" }
    }

}
