package dev.dto

import kotlinx.serialization.Serializable

@Serializable
data class CreateGameRequestDto(
    private val maxRounds: Int,
    private val maxPlayers: Int,
) {
    init {
        require(maxRounds > 0) { "maxRounds must be greater than 0"}
        require(maxPlayers > 0) { "maxPlayers must be greater than 0"}
    }
}