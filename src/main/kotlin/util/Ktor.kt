package util

import config.AppConfig
import core.domainprimitives.DomainPrimitiveException
import core.restadapter.RestAdapterException
import io.ktor.client.*
import io.ktor.client.engine.apache5.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import org.apache.hc.client5.http.HttpHostConnectException
import org.koin.core.component.KoinComponent


class Ktor: KoinComponent {

    val client = HttpClient(Apache5) {
        applyCommonConfiguration()
    }
    companion object{
        fun HttpClientConfig<*>.applyCommonConfiguration(){
            configureContentNegotiation()
            configureLogging()
            configureRetry()
            configureTimeouts()
            HttpResponseValidator {
                handleResponseExceptionWithRequest(::handleThrowable)
                //Similar usage to @ResponseStatus annotation in Spring
                validateResponse(::validateHttpResponse)
            }
        }

        private fun HttpClientConfig<*>.configureContentNegotiation() {
            install(ContentNegotiation) {
                json(Json {
                    prettyPrint = true
                    isLenient = true
                })
            }
        }

        private fun HttpClientConfig<*>.configureTimeouts(){
            install(HttpTimeout) {
                requestTimeoutMillis = 2500
                connectTimeoutMillis = 2500
                socketTimeoutMillis = 2500
            }
        }

        private fun HttpClientConfig<*>.configureRetry() {
            install(HttpRequestRetry) {
                retryOnServerErrors(maxRetries = 1)
            }
        }

        private fun HttpClientConfig<*>.configureLogging() {
            install(Logging) {
                logger = Logger.DEFAULT
                level = AppConfig.ktorLogLevel
            }
        }


        private fun handleThrowable(throwable: Throwable, req: HttpRequest) {
            when (throwable) {
                is HttpHostConnectException -> throw RestAdapterException("Failed to make a request to the Service ${req.url}. Are you sure it's running? Maybe you need to specify the correct host and port in the environment variables.\n $throwable")
                else -> throw throwable
            }
        }

        private fun validateHttpResponse(response: HttpResponse){
            when (response.status) {
                HttpStatusCode.UnprocessableEntity -> throw DomainPrimitiveException()
                else -> Unit
            }
        }
    }




}