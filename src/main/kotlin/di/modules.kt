package di

import DungeonPlayerStartupService
import config.AppConfig
import core.eventlistener.EventDeserializerService
import core.eventlistener.EventHandlerRegistry
import core.eventlistener.EventListenerService
import core.eventlistener.EventProcessorService
import core.restadapter.GameServiceRESTAdapter
import dataendpoint.application.DataEndpointService
import dev.GameAdminClient
import dev.GameAutoInitializer
import game.application.GameApplicationService
import game.application.usecases.GameStatusEventHandler
import game.application.usecases.RoundStatusEventHandler
import game.domain.GameRepository
import game.domain.GameRepositoryImpl
import io.ktor.client.*
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import player.application.PlayerApplicationService
import player.application.usecases.BankAccountClearedEventHandler
import player.application.usecases.BankAccountInitializedEventHandler
import player.application.usecases.PlayerStatusEventHandler
import player.domain.PlayerRepository
import player.domain.PlayerRepositoryImpl
import util.Ktor


val appModule = module {
    singleOf(::Ktor)
    single<HttpClient> { get<Ktor>().client}
    single<AppConfig> { AppConfig }
    singleOf(::DataEndpointService)

    singleOf(::PlayerApplicationService)
    singleOf(::GameApplicationService)
    singleOf(::EventProcessorService)
    singleOf(::EventListenerService)
    singleOf(::EventDeserializerService)
    singleOf(::GameServiceRESTAdapter)

    singleOf(::GameAdminClient)
    singleOf(::GameAutoInitializer)
    singleOf(::DungeonPlayerStartupService)
    singleOf(::EventHandlerRegistry)

    // EventHandlers
    singleOf(::BankAccountClearedEventHandler)
    singleOf(::BankAccountInitializedEventHandler)
    singleOf(::GameStatusEventHandler)
    singleOf(::PlayerStatusEventHandler)
    singleOf(::RoundStatusEventHandler)

    //Repos
    singleOf<PlayerRepository>(::PlayerRepositoryImpl)
    singleOf<GameRepository>(::GameRepositoryImpl)
}

