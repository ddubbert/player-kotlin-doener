package dataendpoint.routes

import dataendpoint.application.DataEndpointService
import dataendpoint.domain.GameDto
import game.application.GameApplicationService
import game.domain.Game
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject
import java.util.*

fun Route.gameRoutes() {
    val gameApplicationService: GameApplicationService by inject()
    get(DataEndpointService.GAMES_PATH) {
        val data: Game? = gameApplicationService.queryActiveGame()
        if (data != null) {
            val gameDto = GameDto.fromGame(data)
            call.respond(gameDto)
        } else {
            call.respondText("No active game found", status = HttpStatusCode.NotFound)
        }
    }

    get("${DataEndpointService.GAMES_PATH}/{id}") {
        val id = call.parameters["id"]
        if (id != null) {
            val data: Game? = gameApplicationService.findGameById(UUID.fromString(id))
            if (data != null) {
                val gameDto = GameDto.fromGame(data)
                call.respond(gameDto)
            } else {
                call.respondText("No game found with id $id", status = HttpStatusCode.NotFound)
            }
        } else {
            call.respondText("Invalid id", status = HttpStatusCode.BadRequest)
        }
    }
}