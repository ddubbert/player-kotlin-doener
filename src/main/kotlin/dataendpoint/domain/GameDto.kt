package dataendpoint.domain

import game.domain.Game
import kotlinx.serialization.Serializable

@Serializable
data class GameDto(
    val gameId: String,
    val gameStatus: String,
    val currentRoundNumber: Int,
){
    companion object {
        fun fromGame(game: Game): GameDto {
            return GameDto(
                gameId = game.gameId.toString(),
                gameStatus = game.gameStatus.toString(),
                currentRoundNumber = game.currentRoundNumber,
            )
        }
    }
}
