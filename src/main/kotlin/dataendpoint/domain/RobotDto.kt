@file:UseSerializers(UUIDSerializer::class)
package dataendpoint.domain

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
data class RobotDto(
    val robotId: UUID,
    val planetId: UUID,
    val playerId: String, // is not UUID, because you can also send enemy robots

    // These are just the minimum values you HAVE TO provide, there is way more you can provide to get a more detailed Map.
    // Checkout the Documentation for more information.
) {

    companion object {
        //TODO add fromRobot : RobotDto function
    }

}