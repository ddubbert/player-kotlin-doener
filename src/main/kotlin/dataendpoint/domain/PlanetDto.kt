@file:UseSerializers(UUIDSerializer::class)
package dataendpoint.domain

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*
@Serializable
data class PlanetDto(
    val planetId: UUID,
    val movementDifficulty: Int,
    val neighbours: List<Neighbour>,
    // These are just the minimum values you HAVE TO provide, there is way more you can provide to get a more detailed Map.
    // Checkout the Documentation for more information.
) {
    companion object {
        //TODO() add fromPlanet to PlanetDto function
    }
}
@Serializable
data class Neighbour(
    val id: UUID,
    val direction: String
)