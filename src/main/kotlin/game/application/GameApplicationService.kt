package game.application

import core.eventlistener.concreteevents.game.GameStatusEvent
import core.restadapter.GameServiceRESTAdapter
import game.domain.Game
import game.domain.GameRepository
import game.domain.GameStatus
import org.koin.core.component.KoinComponent
import org.slf4j.LoggerFactory
import player.application.PlayerApplicationService
import java.util.*

class GameApplicationService(
    private val gameRepository: GameRepository,
    private val gameServiceRESTAdapter: GameServiceRESTAdapter,
    private val playerApplicationService: PlayerApplicationService
) : KoinComponent {
    private val logger = LoggerFactory.getLogger(javaClass)

    /*
    * This method is called when we receive a GameStatusEvent with status CREATED.
    * We join the game and save it to our database.
    * If we are in dev mode, we might receive a CREATED event for a game that was already created and stopped by the Auto Initializer.
    * In this case, we don't join the game and don't save it to our database.
     */
    suspend fun joinAndSaveNewGame(gameStatusEvent: GameStatusEvent) {
        val ourPlayerJoined = playerApplicationService.joinGame(gameStatusEvent.payload.gameId)
        if(!ourPlayerJoined) {
            logger.trace("Our player has not joined the game. This can happen in dev Mode because the Auto Initializer stops previous created games and you still receive the created events for them. ")
            return
        }

        val game = Game(
            gameId = gameStatusEvent.payload.gameId,
            gameStatus = gameStatusEvent.payload.status,
            currentRoundNumber = 0,
        )
        gameRepository.save(game)
    }

    fun changeGameStatus(gameId: UUID, gameStatus: GameStatus) {
        val game = gameRepository.findById(gameId)
        if(game == null) {
            logger.trace("Game {} not found", gameId)
            return
        }
        game.gameStatus = gameStatus
        gameRepository.save(game)
    }

    fun queryActiveGame(): Game? {
        val activeGames = gameRepository.findAllByGameStatusBetween(GameStatus.CREATED, GameStatus.STARTED)
        if(activeGames.size > 1) {
            logger.error("There are more than one active games. This should not happen.")
            return null
        }
        if(activeGames.size == 1) {
            return activeGames[0]
        }

        return null

    }

    fun findGameById(gameId :UUID) : Game?{
        val game = gameRepository.findById(gameId)
        if(game == null) {
            logger.trace("Game {} not found", gameId)
            return null
        }
        return game
    }

    fun roundStarted(currentRoundNumber: Int) {
        val game = queryActiveGame()
        if(game == null) {
            logger.trace("No Active Game found, cant update RoundNumber")
            return
        }
        game.currentRoundNumber = currentRoundNumber
        gameRepository.save(game)
    }
}