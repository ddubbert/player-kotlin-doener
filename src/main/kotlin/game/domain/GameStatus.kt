package game.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class GameStatus {
    @SerialName("created")
    CREATED,
    @SerialName("started")
    STARTED,
    @SerialName("ended")
    FINISHED;

    fun isActive() : Boolean {
        return this == CREATED || this == STARTED
    }

    fun isOpenForJoining() : Boolean {
        return this == CREATED
    }

    fun isRunning() : Boolean {
        return this == STARTED
    }
}