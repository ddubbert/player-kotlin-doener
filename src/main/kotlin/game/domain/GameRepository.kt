package game.domain

import core.CrudRepository
import java.util.*

interface GameRepository : CrudRepository<Game, UUID> {
    fun findAllByGameStatusBetween(gameStatus1: GameStatus, gameStatus2: GameStatus): List<Game>
}