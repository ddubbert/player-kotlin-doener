
import config.AppConfig
import core.eventlistener.EventListenerService
import dataendpoint.application.DataEndpointService
import dev.GameAutoInitializer
import di.appModule
import kotlinx.coroutines.runBlocking
import org.koin.core.context.startKoin
import org.koin.java.KoinJavaComponent.inject

private fun startKoin() {
    startKoin {
        modules(
            listOf(
                appModule
            )
        )
    }
}

private fun setupShutdownHook(autoInitializer: GameAutoInitializer, eventListenerService: EventListenerService) {
    Runtime.getRuntime().addShutdownHook(Thread {
        runBlocking { autoInitializer.stopExistingGamesAndStartNewGame() }
        eventListenerService.purgeCurrentQueue()
    })
}

private fun initializeDevEnvironment(autoInitializer: GameAutoInitializer, eventListenerService: EventListenerService) {
    runBlocking {
        if (AppConfig.dev) {
            autoInitializer.stopExistingGamesAndStartNewGame()
            setupShutdownHook(autoInitializer, eventListenerService)
        }
    }
}

private fun bootstrapApplication() = runBlocking {
    startKoin()

    val autoInitializer: GameAutoInitializer by inject(GameAutoInitializer::class.java)
    val eventListenerService by inject<EventListenerService>(EventListenerService::class.java)
    val dungeonPlayerStartupService by inject<DungeonPlayerStartupService>(DungeonPlayerStartupService::class.java)
    val dataEndpointService by inject<DataEndpointService>(DataEndpointService::class.java)

    initializeDevEnvironment(autoInitializer, eventListenerService)
    dungeonPlayerStartupService.start()
    dataEndpointService.start()

}

fun main() {
    bootstrapApplication()
}
