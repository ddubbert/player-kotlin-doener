package config

import io.ktor.client.plugins.logging.*

object AppConfig {
    val dataEndpointPort : Int = System.getenv("DATA_ENDPOINT_PORT")?.toInt() ?: 8090
    val rabbitMQHost: String = System.getenv("RABBITMQ_HOST") ?: "localhost"
    val rabbitMQPort: Int = System.getenv("RABBITMQ_PORT")?.toInt() ?: 5672
    val rabbitMQUsername: String = System.getenv("RABBITMQ_USERNAME") ?: "admin"
    val rabbitMQPassword: String = System.getenv("RABBITMQ_PASSWORD") ?: "admin"
    val gameHost: String = System.getenv("GAME_HOST") ?: "localhost:8080"
    val playerName: String = System.getenv("PLAYER_NAME") ?: "player-skeleton-kotlin"
    val playerEmail: String = System.getenv("PLAYER_EMAIL") ?: "player-skeleton-kotlin@example.com"
    val ktorLogLevel: LogLevel = System.getenv("KTOR_LOG_LEVEL")?.let { LogLevel.valueOf(it) } ?: LogLevel.INFO
    val dev: Boolean =
        System.getenv("DEV")?.toBoolean() ?: true //If true, the application will create and start a game automatically
}