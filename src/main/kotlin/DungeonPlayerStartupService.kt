import core.eventlistener.EventListenerService
import core.restadapter.PlayerRegistrationResponseDto
import org.koin.core.component.KoinComponent
import org.slf4j.LoggerFactory
import player.application.PlayerApplicationService
import player.domain.Player

class DungeonPlayerStartupService(
    private val playerApplicationService: PlayerApplicationService,
    private val eventListenerService : EventListenerService
) : KoinComponent {
    private val logger = LoggerFactory.getLogger(javaClass)


    suspend fun start() {
        val playerRegistryDto = registerPlayer()
        val player = Player(
            playerId = playerRegistryDto.playerId,
            name = playerRegistryDto.name,
            email = playerRegistryDto.email,
            playerQueue = playerRegistryDto.playerQueue,
            playerExchange = playerRegistryDto.playerExchange
        )
        playerApplicationService.savePlayer(player)
        listenAndProcessIncomingEvents(playerRegistryDto.playerQueue)
    }

    private suspend fun registerPlayer(): PlayerRegistrationResponseDto {
        return playerApplicationService.fetchAndIfNeededCreatePlayer()
    }

    private fun listenAndProcessIncomingEvents(playerQueue: String) {
        eventListenerService.listenAndProcessEvents(playerQueue)
        logger.info("Listening for events on queue: $playerQueue")
    }



}