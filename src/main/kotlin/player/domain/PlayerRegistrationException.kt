package player.domain

import DungeonPlayerRuntimeException

class PlayerRegistrationException(message: String) : DungeonPlayerRuntimeException(message)