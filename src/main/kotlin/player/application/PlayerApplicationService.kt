package player.application

import config.AppConfig
import core.domainprimitives.purchasing.Money
import core.restadapter.GameServiceRESTAdapter
import core.restadapter.PlayerRegistrationResponseDto
import org.koin.core.component.KoinComponent
import org.slf4j.LoggerFactory
import player.domain.Player
import player.domain.PlayerNotFoundException
import player.domain.PlayerRepository
import java.math.BigDecimal
import java.util.*

class PlayerApplicationService(
    private val appConfig: AppConfig,
    private val playerRepository: PlayerRepository,
    private val gameServiceRESTAdapter: GameServiceRESTAdapter
) : KoinComponent {

    private val logger = LoggerFactory.getLogger(javaClass)


    /**
     * Fetch the existing player. If there isn't one yet, it is created and stored to the database.
     * @return The current player.
     */
    suspend fun fetchAndIfNeededCreatePlayer() : PlayerRegistrationResponseDto {
        return try {
            val fetchedPlayerDto = gameServiceRESTAdapter.sendGetRequestForPlayerDto()
            logger.info("Player ${appConfig.playerName} existed already. No need for Reregistration!")
            fetchedPlayerDto
        } catch (e: Exception) {
            when (e) {
                is PlayerNotFoundException -> {
                    registerPlayer(appConfig.playerName, appConfig.playerEmail)
                }
                else -> throw e
            }
        }
    }

    private suspend fun registerPlayer(playerName:String, playerEmail: String) : PlayerRegistrationResponseDto {
        return gameServiceRESTAdapter.registerPlayer(playerName, playerEmail)
    }

    suspend fun joinGame(gameId : UUID) : Boolean {
        val player = fetchAndIfNeededCreatePlayer()
        return gameServiceRESTAdapter.joinGame(
            gameId = gameId,
            playerId = player.playerId
        )
    }

    fun getPlayer(playerId: UUID) : Player? {
        return playerRepository.findById(playerId)
    }
    fun savePlayer(player : Player) {
        playerRepository.save(player)
        logger.info("Saved Player ${player.playerId} to database.")
    }

    fun updatePlayerBankAccount(playerId: UUID, newBalance: BigDecimal){
        val player = playerRepository.findById(playerId)
        if(player == null) {
            logger.error("Player {} not found", playerId)
            return
        }
        player.money = Money.from(newBalance)
        playerRepository.save(player)
    }




}