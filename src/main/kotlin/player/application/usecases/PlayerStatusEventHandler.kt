package player.application.usecases

import core.eventlistener.EventHandler
import core.eventlistener.concreteevents.player.PlayerStatusEvent
import org.koin.core.component.KoinComponent
import player.application.PlayerApplicationService
import player.domain.Player

/*
Since we only have all relevant info for a Player after we received the PlayerStatusEvent, we need to save the Player to our database here.
 */
class PlayerStatusEventHandler(private val playerApplicationService: PlayerApplicationService) :
    EventHandler<PlayerStatusEvent>, KoinComponent {
    override suspend fun handle(event: PlayerStatusEvent) {
        val existingPlayer = playerApplicationService.getPlayer(event.payload.playerId)
        if (existingPlayer != null) {
            existingPlayer.gameId = event.payload.gameId
            playerApplicationService.savePlayer(existingPlayer)
            return
        } else {
            val newPlayer = Player(
                playerId = event.payload.playerId,
                gameId = event.payload.gameId,
            )
            playerApplicationService.savePlayer(newPlayer)
        }
    }
}