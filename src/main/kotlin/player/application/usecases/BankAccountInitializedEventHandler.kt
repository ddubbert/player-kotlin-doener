package player.application.usecases

import core.eventlistener.EventHandler
import core.eventlistener.concreteevents.trading.BankAccountInitializedEvent
import org.slf4j.LoggerFactory
import player.application.PlayerApplicationService

class BankAccountInitializedEventHandler(private val playerApplicationService: PlayerApplicationService) : EventHandler<BankAccountInitializedEvent> {
    private val logger = LoggerFactory.getLogger(javaClass)
    override suspend fun handle(event: BankAccountInitializedEvent) {
        logger.info("Handling BankAccountInitialized event for bank account: ${event.payload.playerId} balance is now ${event.payload.balance}")
        playerApplicationService.updatePlayerBankAccount(event.payload.playerId, event.payload.balance)
    }

}
