@file:UseSerializers(BigDecimalSerializer::class)
package core.domainprimitives.purchasing

import core.domainprimitives.DomainPrimitiveException
import core.eventlistener.serializers.BigDecimalSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.math.BigDecimal

@Serializable
data class Money protected constructor(
    private val amount: BigDecimal //This is the Type Money is based on in the core services.
) {
    companion object {
        fun from(amountAsBigDecimal: BigDecimal): Money {
            if (amountAsBigDecimal < BigDecimal.ZERO) throw DomainPrimitiveException("Amount must be >= 0!")
            return Money(amountAsBigDecimal)
        }

        fun zero(): Money {
            return from(BigDecimal.ZERO)
        }
    }

    fun canBuyThatManyFor(price: Money): BigDecimal {
        if (price.amount <= BigDecimal.ZERO) throw DomainPrimitiveException("Price must be > 0!")
        return this.amount.divide(price.amount)
    }

    fun decreaseBy(moneyDue: Money): Money {
        if (moneyDue.amount > this.amount) throw DomainPrimitiveException("Not enough money!")
        return Money(this.amount.subtract(moneyDue.amount))
    }

    fun increaseBy(additional: Money): Money {
        return Money(this.amount.add(additional.amount))
    }

    fun greaterThan(otherMoney: Money): Boolean {
        return this.amount > otherMoney.amount
    }

    fun greaterEqualThen(otherMoney: Money): Boolean {
        return this.amount >= otherMoney.amount
    }
}