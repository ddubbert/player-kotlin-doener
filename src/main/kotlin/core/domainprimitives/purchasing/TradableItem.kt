package core.domainprimitives.purchasing

import kotlinx.serialization.Serializable

@Serializable
data class TradableItem (
    val name: String,
    val price : Int,
    val type : TradableType,
)