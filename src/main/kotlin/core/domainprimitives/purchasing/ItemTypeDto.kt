package core.domainprimitives.purchasing

import kotlinx.serialization.Serializable

@Serializable
enum class ItemTypeDto {
    HEALTH_RESTORE,
    ENERGY_RESTORE,
    ROBOT
}