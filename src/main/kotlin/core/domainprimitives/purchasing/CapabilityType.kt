package core.domainprimitives.purchasing

import kotlinx.serialization.Serializable

@Serializable
enum class CapabilityType(private val stringValue: String) {
    DAMAGE("DA"),
    ENERGY_REGEN("ER"),
    HEALTH("H"),
    MAX_ENERGY("ME"),
    MINING("MI"),
    MINING_SPEED("MS"),
    STORAGE("S");

    override fun toString(): String {
        return stringValue
    }


}

