package core.domainprimitives

import DungeonPlayerRuntimeException

class DomainPrimitiveException(override val message: String = "Invalid Domain Primitive") : DungeonPlayerRuntimeException(message)