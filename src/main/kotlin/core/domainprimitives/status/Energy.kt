package core.domainprimitives.status

import core.domainprimitives.DomainPrimitiveException

data class Energy protected constructor(
    private val energyAmount : Int
) {
    companion object {
        fun from (energyAmount : Int) : Energy {
            if (energyAmount < 0) throw DomainPrimitiveException("Energy must be >= 0!")
            return Energy(energyAmount)
        }

        fun zero() : Energy {
            return from(0)
        }

        fun defaultMovementDifficulty() : Energy {
            return from(1)
        }

        fun initialRobotEnergy() : Energy {
            return from(20)
        }
    }

    fun decreaseBy ( energyDue: Energy) : Energy {
        if (energyDue.energyAmount > this.energyAmount) throw DomainPrimitiveException("Negative Energy not allowed!")
        return from(this.energyAmount - energyDue.energyAmount)
    }

    fun increaseBy( additional : Energy) : Energy {
        return from(this.energyAmount + additional.energyAmount)
    }

    fun greaterThan( otherEnergy: Energy) : Boolean {
        return this.energyAmount > otherEnergy.energyAmount
    }

    fun greaterEqualThen( otherEnergy: Energy) : Boolean {
        return this.energyAmount >= otherEnergy.energyAmount
    }

    fun lowerThanPercentage(percentage: Int, comparisonEnergy: Energy) : Boolean {
        if (percentage < 0 || percentage > 100) throw DomainPrimitiveException("Percentage must be between 0 and 100!")
        val fraction = comparisonEnergy.energyAmount.toFloat() * percentage.toFloat() / 100f
        return energyAmount.toFloat() < fraction
    }
}
