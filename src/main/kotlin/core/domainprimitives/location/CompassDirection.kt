package core.domainprimitives.location

import kotlinx.serialization.Serializable
import java.util.*

@Serializable
enum class CompassDirection {
    NORTH,
    EAST,
    SOUTH,
    WEST;

    fun getOppositeDirection() : CompassDirection {
       return when(this) {
            NORTH -> SOUTH
            EAST -> WEST
            SOUTH -> NORTH
            WEST -> EAST
       }
    }

    fun xOffset(): Int {
        return when (this) {
            NORTH -> 0
            EAST -> 1
            SOUTH -> 0
            WEST -> -1
        }
    }

    fun yOffset(): Int {
        return when (this) {
            NORTH -> 1
            EAST -> 0
            SOUTH -> -1
            WEST -> 0
        }
    }

    fun ninetyDegrees(): List<CompassDirection> {
        val retVals: MutableList<CompassDirection> = ArrayList()
        when (this) {
            NORTH -> {
                retVals.add(WEST)
                retVals.add(EAST)
            }

            EAST -> {
                retVals.add(NORTH)
                retVals.add(SOUTH)
            }

            SOUTH -> {
                retVals.add(WEST)
                retVals.add(EAST)
            }

            WEST -> {
                retVals.add(NORTH)
                retVals.add(SOUTH)
            }
        }
        return retVals
    }

    fun toStringShort(): String {
        return toString().substring(0, 1)
    }

    companion object {
        fun random(): CompassDirection {
            val random = Random()
            return entries[random.nextInt(4)]
        }
    }
}
