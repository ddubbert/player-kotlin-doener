package core.domainprimitives.location

import core.domainprimitives.DomainPrimitiveException
import kotlinx.serialization.Serializable

@Serializable
data class MinableResource protected constructor(
    private val type: MinableResourceType,
    private val amount: Int
) {

    fun add(additionalResource: MinableResource?): MinableResource {
        if (additionalResource == null) throw DomainPrimitiveException("additionalResource cannot be null!")
        if (additionalResource.isEmpty) return this
        if (isEmpty) return additionalResource
        if (type !== additionalResource.type) throw DomainPrimitiveException("Cannot add resources of different types!")
        return MinableResource(type, amount + additionalResource.amount)
    }

    val isEmpty: Boolean
        get() = amount == 0

    companion object {
        fun fromTypeAndAmount(minableResourceType: MinableResourceType?, amount: Int?): MinableResource {
            if (minableResourceType == null) throw DomainPrimitiveException("MineableResourceType cannot be null!")
            if (amount == null) throw DomainPrimitiveException("Amount cannot be null!")
            if (amount <= 0) throw DomainPrimitiveException("Amount must be > 0!")
            return MinableResource(minableResourceType, amount)
        }
    }
}
