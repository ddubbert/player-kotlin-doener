@file:UseSerializers(UUIDSerializer::class)
package core.domainprimitives.command

import core.domainprimitives.DomainPrimitiveException
import core.domainprimitives.purchasing.Capability
import core.domainprimitives.purchasing.ItemTypeDto
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
data class Command protected constructor(
    val playerId : UUID,
    @SerialName("type")
    val commandType : CommandType,
    @SerialName("data")
    val commandObject : CommandObject
) {
    companion object {
        fun createMove( robotId : UUID, planetId: UUID, playerId : UUID) : Command {
            val movementCommandObject = CommandObject(robotId, planetId)
            return Command(playerId, CommandType.MOVEMENT, movementCommandObject)
        }

        fun createItemPurchase(playerId : UUID, robotId: UUID, itemTypeDto : ItemTypeDto, amount : Int) : Command {
            if(amount < 0) throw DomainPrimitiveException("amount < 0")
            val buyingCommandObject = CommandObject(robotId, itemName = itemTypeDto.name, itemQuantity = amount)
            return Command(playerId, CommandType.BUYING, buyingCommandObject)
        }

        fun createRobotPurchase(playerId : UUID, amount: Int) : Command {
            val robotItemName = "ROBOT"
            if(amount < 0) throw DomainPrimitiveException("amount < 0")
            val buyingCommandObject = CommandObject(robotId = playerId, itemName = robotItemName, itemQuantity = amount)
            return Command(playerId, CommandType.BUYING, buyingCommandObject)
        }

        fun createRobotUpgrade(playerId : UUID, capability: Capability, robotId: UUID ) : Command {
            val buyingCommandObject = CommandObject(robotId, itemName = capability.nextLevel().toStringForCommand(), itemQuantity = 1)
            return Command(playerId, CommandType.BUYING, buyingCommandObject)
        }

        fun createRobotRegeneration(playerId : UUID, robotId: UUID) : Command {
            val regenerationCommandObject = CommandObject(robotId)
            return Command(playerId, CommandType.REGENERATE, regenerationCommandObject)
        }

        fun createMining(playerId : UUID, robotId: UUID, planetId: UUID ) : Command {
            val miningCommandObject = CommandObject(robotId, planetId)
            return Command(playerId, CommandType.MINING, miningCommandObject)
        }

        fun createSellInventory( playerId : UUID, robotId: UUID) : Command {
            val sellingCommandObject = CommandObject(robotId)
            return Command(playerId, CommandType.SELLING, sellingCommandObject)
        }
    }

    override fun toString(): String {
        return buildString {
            append(commandType.name)
            append(" R:").append(commandObject.robotId.toString().take(4))
            append(" P:").append(commandObject.planetId.toString().take(4))
            append(" I:").append(commandObject.itemName)
        }
    }

}
