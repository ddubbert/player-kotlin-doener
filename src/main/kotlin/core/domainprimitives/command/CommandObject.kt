@file:UseSerializers(UUIDSerializer::class)
package core.domainprimitives.command

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*
@Serializable
data class CommandObject(
    val robotId: UUID,
    val planetId: UUID? = null,
    val targetId : UUID? = null,
    val itemName: String? = null,
    val itemQuantity: Int? = null
)
