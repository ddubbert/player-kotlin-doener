package core.restadapter

import core.eventlistener.serializers.UUIDSerializer
import game.domain.GameStatus
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class GameInfoResponseDto(
    @Serializable(with = UUIDSerializer::class)
    val gameId: UUID,
    val gameStatus: GameStatus,
    val maxPlayers: Int,
    val maxRounds: Int,
    val currentRoundNumber: Int?,
    val roundLengthInMillis: Long,
    val participatingPlayers: ArrayList<String>
)