package core.restadapter

import config.AppConfig
import core.domainprimitives.command.Command
import game.domain.GameDoesNotExistException
import game.domain.GameStatus
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import org.koin.core.component.KoinComponent
import org.slf4j.LoggerFactory
import player.domain.PlayerNotFoundException
import player.domain.PlayerRegistrationException
import java.util.*

class GameServiceRESTAdapter(
    private val appConfig: AppConfig,
    private val httpClient: HttpClient
) : KoinComponent {

    private val logger = LoggerFactory.getLogger(javaClass)
    suspend fun fetchForJoinableGame(): GameInfoResponseDto? {
        val openGames = fetchAllGames()
        return openGames.firstOrNull { game ->
            game.gameStatus == GameStatus.CREATED && !game.participatingPlayers.contains(
                appConfig.playerName
            )
        }
    }

    suspend fun fetchAllGames(): List<GameInfoResponseDto> {
        val response = httpClient.get {
            url("http://${appConfig.gameHost}/games")
            contentType(ContentType.Application.Json)
        }
        return if (response.status == HttpStatusCode.OK) {
            response.body<List<GameInfoResponseDto>>()
        } else {
            throw RestAdapterException("Error fetching open games: ${response.status} ${response.bodyAsText()}")
        }
    }

    suspend fun sendGetRequestForPlayerDto(): PlayerRegistrationResponseDto {
        val response = httpClient.get {
            url("http://${appConfig.gameHost}/players")
            contentType(ContentType.Application.Json)
            url {
                parameters.append("name", appConfig.playerName)
                parameters.append("mail", appConfig.playerEmail)
            }
        }
        return when (response.status) {
            HttpStatusCode.OK -> response.body<PlayerRegistrationResponseDto>()
            HttpStatusCode.NotFound -> throw PlayerNotFoundException("Player with name ${appConfig.playerName} and email : ${appConfig.playerEmail} was not found!")
            else -> throw PlayerRegistrationException("Unexpected response when trying to fetch player: ${response.status}")
        }

    }

    suspend fun joinGame(gameId: UUID, playerId: UUID): Boolean {
        val response = httpClient.put {
            url("http://${appConfig.gameHost}/games/${gameId}/players/${playerId}")
            contentType(ContentType.Application.Json)
        }
        return when (response.status) {
            HttpStatusCode.OK -> {
                logger.info("Joined game successfully.")
                true
            }

            HttpStatusCode.BadRequest -> {
                logger.error("Failed to join game. Its either full or has been started: ${response.status}")
                false
            }

            HttpStatusCode.NotFound -> {
                logger.error("Failed to join game. Player or Game not found ${response.status} ")
                false
            }

            else -> {
                logger.error("Failed to join game. ${response.status} \n ${response.bodyAsText()}")
                false
            }
        }
    }

    suspend fun registerPlayer(
        playerName: String = appConfig.playerName,
        playerEmail: String = appConfig.playerEmail
    ): PlayerRegistrationResponseDto {
        val response = httpClient.post {
            url("http://${appConfig.gameHost}/players")
            contentType(ContentType.Application.Json)
            setBody(PlayerRegistrationRequestDto(playerName, playerEmail))
        }
        if (response.status == HttpStatusCode.Created) {
            logger.info("Player with name $playerName and email : $playerEmail was successfully registered!")
            return response.body<PlayerRegistrationResponseDto>()
        } else {
            logger.error("Error registering player: ${response.status}")
            throw PlayerRegistrationException("Player could not be found and not be registered. This should never happen. ${response.status} \n ${response.body<String>()}")
        }
    }


    suspend fun hasGameStarted(gameId: UUID): Boolean {
        fetchAllGames().firstOrNull { game -> game.gameId == gameId }?.let { game ->
            return game.gameStatus == GameStatus.STARTED
        } ?: throw GameDoesNotExistException(gameId)

    }

    suspend fun sendPostRequestForCommand(command: Command): CommandResponseDto? {
        val response = httpClient.post {
            url("http://${appConfig.gameHost}/commands")
            contentType(ContentType.Application.Json)
            setBody(command)
        }
        var commandResponseDto: CommandResponseDto? = null
        when (response.status) {
            HttpStatusCode.Created -> {
                commandResponseDto = response.body<CommandResponseDto>()
                logger.info("Command ${commandResponseDto.transactionId} $command was successfully sent!")
            }

            HttpStatusCode.BadRequest -> {
                throw RestAdapterException("Command syntax Error! Command was $command \n ${response.status} \n ${response.bodyAsText()}")
            }

            HttpStatusCode.NotFound -> {
                throw RestAdapterException("Player or Game not found: ${response.status} \n ${response.bodyAsText()}")
            }
        }
        return commandResponseDto

    }
}