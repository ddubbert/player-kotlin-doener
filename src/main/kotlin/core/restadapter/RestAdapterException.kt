package core.restadapter

import DungeonPlayerRuntimeException

class RestAdapterException(message : String) : DungeonPlayerRuntimeException(message)