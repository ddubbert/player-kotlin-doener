package core.restadapter

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class CommandResponseDto(
    @Serializable(with = UUIDSerializer::class)
    val transactionId: UUID
)