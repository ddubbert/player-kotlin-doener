package core.restadapter

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import java.util.*

/**
 * DTO for the response type that GameService sends back as an answer to all kinds of
 * commands: Just with a transactionId in it.
 */
@Serializable
data class TransactionIdResponseDto(
    @Serializable(with = UUIDSerializer::class)
    private val transactionId: UUID?
) {
    fun isValid() : Boolean = transactionId != null

}