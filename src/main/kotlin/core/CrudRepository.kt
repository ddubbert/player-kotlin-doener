package core

/**
 * The CrudRepository interface provides the basic CRUD (Create, Read, Update, Delete) operations for managing entities of type [T] with an identifier of type [ID].
 *
 * @param T the type of the entity this repository manages.
 * @param ID the type of the identifier of the entity this repository manages.
 */
interface CrudRepository<T, ID> {

    fun <S : T> save(entity: S): S

    fun <S : T> saveAll(entities: Iterable<S>): Iterable<S>

    fun getAll(): List<T>

    fun findById(id: ID): T?

    fun existsById(id: ID): Boolean

    fun findAll(predicate : (T) -> Boolean): List<T>

    fun findAllById(ids: Iterable<ID>): List<T>

    fun count(): Long

    fun deleteById(id: ID)

    fun delete(entity: T)

    fun deleteAllById(ids: Iterable<ID>)

    fun deleteAll(entities: Iterable<T>)

    fun deleteAll()
}

