package core.eventlistener

import DungeonPlayerRuntimeException
import com.rabbitmq.client.*
import config.AppConfig
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.*
import org.koin.core.component.KoinComponent
import org.slf4j.LoggerFactory
import java.io.Closeable
import java.net.ConnectException

class EventListenerService(
    private val deserializer: EventDeserializerService,
    private val eventProcessor: EventProcessorService,
) : KoinComponent, Closeable {
    companion object {
        const val UNKNOWN_TYPE = "Unknown Type"
        const val UNKNOWN_TRANSACTION_ID = "Unknown TransactionId"
        const val UNKNOWN_PLAYER_ID = "Unknown PlayerId"
    }

    private val logger = LoggerFactory.getLogger(javaClass)
    private val connectionFactory = createConnectionFactory()
    private val connection: Connection = createConnection()
    private val channel: Channel = createChannel()
    private val jsonMapper = createJsonMapper()
    private val standardQueueName = "player-${AppConfig.playerName}"

    // For Log Formatting
    private val footer = "=".repeat(40)
    private val headerText = "Event Log".center(footer.length)


    private fun createJsonMapper() = Json {
        ignoreUnknownKeys = false
        prettyPrint = true
    }

    private fun createConnectionFactory(
        host: String = AppConfig.rabbitMQHost,
        port: Int = AppConfig.rabbitMQPort,
        username: String = AppConfig.rabbitMQUsername,
        password: String = AppConfig.rabbitMQPassword
    ): ConnectionFactory {
        return ConnectionFactory().apply {
            this.host = host
            this.port = port
            this.username = username
            this.password = password
        }
    }

    private fun createConnection(): Connection {
        try {
            return connectionFactory.newConnection()
        } catch (e: Exception) {
            when (e) {
                is ConnectException -> {
                    throw RabbitMQNotAvailableException("Could not connect to RabbitMQ, is RabbitMQ running?")
                }

                else -> {
                    logger.error("Failed to create connection")
                    throw e
                }
            }
        }
    }

    private fun createChannel(): Channel {
        try {
            return connection.createChannel()
        } catch (e: Exception) {
            when (e) {
                is ConnectException -> {
                    throw RabbitMQNotAvailableException("Could not connect to RabbitMQ, is RabbitMQ Service online?")
                }

                else -> {
                    logger.error("Failed to create channel")
                    throw e
                }
            }
        }
    }

    fun purgeCurrentQueue(): AMQP.Queue.PurgeOk = channel.queuePurge(standardQueueName)

    private fun consumeEvents(queue: String) {
        val consumer = object : DefaultConsumer(channel) {
            override fun handleDelivery(
                consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: ByteArray
            ) {
                handleEventDelivery(properties, body)
            }
        }
        try {
            channel.basicConsume(queue, true, consumer)
        } catch (e: Exception) {
            logger.error("Failed to consume events from queue: $queue", e)
        }
    }

    fun listenAndProcessEvents(queue: String) {
        consumeEvents(queue)
    }


    private fun handleEventDelivery(properties: AMQP.BasicProperties, body: ByteArray) {
        val headersMap = prepareHeadersMap(properties)
        val payload = jsonMapper.parseToJsonElement(String(body, Charsets.UTF_8))
        val prettyMessage = try {
            jsonMapper.encodeToString(payload)
        } catch (e: Exception) {
            logger.error("Failed to pretty print the message.", e)
            payload.toString()
        }
        logEvent(
            type = headersMap["type"] ?: UNKNOWN_TYPE,
            transactionId = headersMap["transactionId"] ?: UNKNOWN_TRANSACTION_ID,
            playerId = headersMap["playerId"] ?: UNKNOWN_PLAYER_ID,
            prettyMessage
        )
        deserializeEventAndProcess(payload, headersMap)
    }


    private fun prepareHeadersMap(properties: AMQP.BasicProperties): MutableMap<String, String> {
        return properties.headers.mapValues {
            String(it.value as ByteArray, Charsets.UTF_8)
        }.toMutableMap().apply {
            if (this["transactionId"].isNullOrBlank() || this["transactionId"] == "null") {
                this["transactionId"] = "2f5bf0c3-6df7-4662-aebe-4d2982d1a3e4"
                logger.warn("TransactionId was null or blank for event type: ${this["type"]}.")
            }
        }
    }

    private fun prettifyMessage(rawMessage: String): String {
        return try {
            //imagine raw message is a byteArray
            val messageElement = jsonMapper.parseToJsonElement(rawMessage)
            jsonMapper.encodeToString(messageElement)
        } catch (e: RuntimeException) {
            logger.error("Failed to pretty print the message: $rawMessage", e)
            rawMessage
        }
    }

    private fun deserializeEventAndProcess(payload: JsonElement, headersMap: Map<String, String>) {
        val mergedJson: JsonElement = mergeJson(headersMap, payload)
        try {
            val event: GameEvent = deserializer.deserializeJsonElementToEvent(mergedJson)
            eventProcessor.processEvent(event)
        } catch (e: RuntimeException) {
            logger.error("Failed to deserialize the event. Merged JSON: $mergedJson", e)
        }
    }

    private fun String.center(totalWidth: Int): String {
        val paddingSize = (totalWidth - this.length) / 2
        val repeatedChar = " "
        return repeatedChar.repeat(paddingSize) + this + repeatedChar.repeat(totalWidth - this.length - paddingSize)
    }

    private fun logEvent(type: String, transactionId: String, playerId: String, payload: String) {


        logger.info(
            """

$footer
$headerText
EventType       : $type
TransactionId   : $transactionId
PlayerId        : $playerId
$footer
Payload:
$payload
$footer

        """
        )
    }

    private fun mergeJson(headerMap: Map<String, String>, payloadElement: JsonElement): JsonElement {
        val payloadAsObject = when (payloadElement) {
            is JsonArray -> buildJsonObject {
                payloadElement.forEachIndexed { index, jsonElement ->
                    put(index.toString(), jsonElement)
                }
            }

            is JsonObject -> payloadElement
            else -> throw DungeonPlayerRuntimeException("Unexpected event payloadElement type: ${payloadElement::class}. Should've been JsonObject or JsonArray.")
        }

        return buildJsonObject {
            put("header", Json.encodeToJsonElement(headerMap))
            put("payload", payloadAsObject)
        }
    }

    override fun close() {
        channel.close()
        connection.close()
    }
}
