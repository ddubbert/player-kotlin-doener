package core.eventlistener

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
@Polymorphic
@Serializable
abstract class GameEvent {
     abstract val header: GameEventHeader
}





