@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
data class GameEventHeader(
    val type: String,
    val eventId: UUID,
    @SerialName("kafka-topic")
    val kafkaTopic: String = "",
    val transactionId: UUID,
    val version: Int,
    val playerId : String?,
    val timestamp: Instant
)
