package core.eventlistener


import core.eventlistener.serializers.GameEventSerializer
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.serializer
import kotlin.reflect.KClass

@OptIn(InternalSerializationApi::class)
class EventDeserializerService {
    private val jsonMapper: Json
    init {
        val module = SerializersModule {
            polymorphic(GameEvent::class) {
                GameEventSerializer.typeToClassMapping.forEach { (type, kClass) ->
                    @Suppress("UNCHECKED_CAST")
                    val kSerializer = kClass.serializer() as KSerializer<GameEvent>
                    subclass(kClass as KClass<GameEvent>, kSerializer)
                }
            }
        }

        jsonMapper = Json {
            serializersModule = module
            coerceInputValues = true
            ignoreUnknownKeys = false
        }
    }

    fun deserializeJsonElementToEvent(json: JsonElement): GameEvent {
        return jsonMapper.decodeFromJsonElement(GameEventSerializer,json)
    }

    fun deserializeJsonStringToEvent(json: String): GameEvent {
        return jsonMapper.decodeFromString(GameEventSerializer,json)
    }
}

