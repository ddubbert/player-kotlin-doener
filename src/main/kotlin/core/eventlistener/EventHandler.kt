package core.eventlistener

interface EventHandler<T: GameEvent> {
    suspend fun handle(event: T)
}