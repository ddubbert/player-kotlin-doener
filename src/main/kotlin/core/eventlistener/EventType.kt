package core.eventlistener

//Usually this would be an Enum, but since Enums are not compile time constants I cannot use them in the @Serialname annotations for the Serializer
object EventType {
    const val GAME_STATUS = "GameStatus"
    const val ROUND_STATUS = "RoundStatus"
    const val PLAYER_STATUS = "PlayerStatus"
    const val BANK_ACCOUNT_CLEARED = "BankAccountCleared"
    const val BANK_ACCOUNT_INITIALIZED = "BankAccountInitialized"
    const val BANK_ACCOUNT_TRANSACTION_BOOKED = "BankAccountTransactionBooked"
    const val TRADABLE_PRICES = "TradablePrices"

    //robot events
    const val ROBOT_SPAWNED = "RobotSpawned"
    const val ROBOT_RESOURCE_MINED = "RobotResourceMined"
    const val ROBOT_REGENERATED = "RobotRegenerated"
    const val ROBOT_MOVED = "RobotMoved"
    const val ROBOTS_REVEALED = "RobotsRevealed"

    //planet events
    const val PLANET_DISCOVERED = "PlanetDiscovered"

    const val UNKNOWN = "UNKNOWN"
    const val ERROR = "error"




    // Add new event types here

}


