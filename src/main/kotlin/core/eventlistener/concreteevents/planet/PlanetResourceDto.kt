package core.eventlistener.concreteevents.planet

import core.domainprimitives.location.MinableResourceType
import kotlinx.serialization.Serializable

@Serializable
data class PlanetResourceDto (
    private val resourceType: MinableResourceType? = null,
    private val maxAmount: Int? = null,
    private val currentAmount: Int? = null
)

