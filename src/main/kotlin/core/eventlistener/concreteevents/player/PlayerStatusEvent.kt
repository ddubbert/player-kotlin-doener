@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.player

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
@SerialName(EventType.PLAYER_STATUS)
data class PlayerStatusEvent(
    override val header: GameEventHeader,
    val payload: PlayerStatusPayload
) : GameEvent()

@Serializable
data class PlayerStatusPayload(
    val playerId: UUID,
    val gameId: UUID,
    val name: String
)

