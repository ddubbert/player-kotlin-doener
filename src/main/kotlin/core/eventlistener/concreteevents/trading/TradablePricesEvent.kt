package core.eventlistener.concreteevents.trading

import core.domainprimitives.purchasing.TradableItem
import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.TradablePricesEventSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable(with = TradablePricesEventSerializer::class)
@SerialName(EventType.TRADABLE_PRICES)
data class TradablePricesEvent (
    override val header: GameEventHeader,
    val payload: List<TradableItem>
) : GameEvent()


