@file:UseSerializers(UUIDSerializer::class, BigDecimalSerializer::class)
package core.eventlistener.concreteevents.trading

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.BigDecimalSerializer
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.math.BigDecimal
import java.util.*

@Serializable
@SerialName(EventType.BANK_ACCOUNT_INITIALIZED)
data class BankAccountInitializedEvent (
    override val header: GameEventHeader,
    val payload: BankAccountInitializedPayload
) : GameEvent()
@Serializable
data class BankAccountInitializedPayload (
    val playerId: UUID,
    val balance: BigDecimal
)