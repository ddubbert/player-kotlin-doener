@file:UseSerializers(UUIDSerializer::class)

package core.eventlistener.concreteevents.trading

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
@SerialName(EventType.BANK_ACCOUNT_TRANSACTION_BOOKED)
data class BankAccountTransactionBookedEvent(
    override val header: GameEventHeader,
    val payload: BankAccountTransactionBookedPayload
) : GameEvent()

@Serializable
data class BankAccountTransactionBookedPayload(
    val playerId: UUID,
    val balance: Double,
    val transactionAmount: Int,
)