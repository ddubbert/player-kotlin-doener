package core.eventlistener.concreteevents.game

import kotlinx.serialization.Serializable

@Serializable
data class ImpreciseTimings(
    val roundStarted : String,
    val commandInputEnded : String?,
    val roundEnded : String?
)
