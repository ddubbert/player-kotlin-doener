@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.robot

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
@SerialName(EventType.ROBOT_REGENERATED)
data class RobotRegeneratedEvent(
    override val header: GameEventHeader,
    val payload: RobotRegeneratedPayload
) : GameEvent()

@Serializable
data class RobotRegeneratedPayload(
    val robotId : UUID,
    val availableEnergy : Int,
)