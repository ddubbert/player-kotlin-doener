@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.robot.reveal

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
data class RobotsRevealedDto(
    private val robotId: UUID,
    private val planetId: UUID,
    private val playerNotion: String,
    private val health : Int,
    private val energy : Int,
    private val levels : Int
)
