@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.robot.spawn

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*
@Serializable
data class RobotPlanetDto(
    private val planetId : UUID,
    private val gameWorldId : UUID,
    private val movementDifficulty: Int,
    private val resourceType: String
)
