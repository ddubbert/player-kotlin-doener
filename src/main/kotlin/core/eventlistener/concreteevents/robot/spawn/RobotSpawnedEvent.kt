@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.robot.spawn

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*
@Serializable
@SerialName(EventType.ROBOT_SPAWNED)
data class RobotSpawnedEvent(
    override val header: GameEventHeader,
    val payload: RobotSpawnedPayload
) : GameEvent()
@Serializable
data class RobotSpawnedPayload(
    val playerId : UUID,
    @SerialName("robot")
    val robotDto : RobotDto
)
