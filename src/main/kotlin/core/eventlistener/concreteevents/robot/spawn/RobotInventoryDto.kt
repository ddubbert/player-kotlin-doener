package core.eventlistener.concreteevents.robot.spawn

import kotlinx.serialization.Serializable

@Serializable
data class RobotInventoryDto(
    private val storageLevel: Int,
    private val usedStorage : Int,
    private val resources: RobotInventoryResourcesDto,
    private val full : Boolean,
    private val maxStorage: Int
)


