@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.robot.spawn
import core.domainprimitives.purchasing.Capability.Companion.MIN_LEVEL
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
data class RobotDto (
    private val player: UUID,
    private val planet: RobotPlanetDto,
    private val id: UUID,
    private val alive : Boolean,

    private val inventory: RobotInventoryDto,

    private val health: Int,
    private val energy: Int,
    private val healthLevel: Int = MIN_LEVEL,
    private val damageLevel: Int = MIN_LEVEL,
    private val miningSpeedLevel: Int = MIN_LEVEL,
    private val miningLevel: Int = MIN_LEVEL,
    private val energyLevel: Int = MIN_LEVEL,
    private val energyRegenLevel: Int = MIN_LEVEL,
    private val miningSpeed: Int,
    private val maxHealth: Int,
    private val maxEnergy: Int,
    private val energyRegen: Int,
    private val attackDamage: Int
)