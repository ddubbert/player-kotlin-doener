package core.eventlistener.concreteevents.robot.mine

import core.domainprimitives.location.MinableResource
import core.domainprimitives.location.MinableResourceType
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RobotResourceInventoryDto(
    @SerialName("COAL")
    private val coal : Int,
    @SerialName("IRON")
    private val iron : Int,
    @SerialName("GEM")
    private val gem : Int,
    @SerialName("GOLD")
    private val gold : Int,
    @SerialName("PLATIN")
    private val platin : Int,
) {
    fun getResource() : MinableResource? {
        if (coal > 0) return MinableResource.fromTypeAndAmount(MinableResourceType.COAL, coal)
        if (iron > 0) return MinableResource.fromTypeAndAmount(MinableResourceType.IRON, iron)
        if (gem > 0) return MinableResource.fromTypeAndAmount(MinableResourceType.GEM, gem)
        if (gold > 0) return MinableResource.fromTypeAndAmount(MinableResourceType.GOLD, gold)
        return if (platin > 0) MinableResource.fromTypeAndAmount(MinableResourceType.PLATIN, platin) else null;
    }
}
