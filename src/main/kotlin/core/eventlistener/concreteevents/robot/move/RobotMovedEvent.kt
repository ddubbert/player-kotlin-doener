@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.robot.move

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*
@Serializable
@SerialName(EventType.ROBOT_MOVED)
data class RobotMovedEvent(
    override val header: GameEventHeader,
    val payload : RobotMovedPayload
) : GameEvent()

@Serializable
data class RobotMovedPayload(
    private val robotId : UUID,
    private val remainingEnergy: Int,
    private val fromPlanet: RobotMovedPlanetDto,
    private val toPlanet: RobotMovedPlanetDto
)