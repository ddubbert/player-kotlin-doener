package core.eventlistener

import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory

class EventProcessorService(
    private val eventHandlerRegistry : EventHandlerRegistry
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun processEvent(event: GameEvent) {
        processEventImpl(event)
    }

    private fun <E : GameEvent> processEventImpl(event: E) {
        runBlocking {
            val handler: EventHandler<E>? = eventHandlerRegistry.getEventHandlerForEvent(event::class)
            if (handler == null) {
                logger.error("No Event Handler found for event: ${event::class.simpleName}")
            } else {
                handler.handle(event)
            }
        }
    }
}
