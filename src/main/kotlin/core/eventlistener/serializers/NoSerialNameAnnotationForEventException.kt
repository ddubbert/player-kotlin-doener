package core.eventlistener.serializers

import DungeonPlayerRuntimeException

class NoSerialNameAnnotationForEventException(message : String) : DungeonPlayerRuntimeException(message)