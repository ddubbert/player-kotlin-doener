package core.eventlistener.serializers

import core.domainprimitives.purchasing.TradableItem
import core.eventlistener.EventType
import core.eventlistener.GameEventHeader
import core.eventlistener.concreteevents.trading.TradablePricesEvent
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object TradablePricesEventSerializer : KSerializer<TradablePricesEvent> {
    private val gameEventHeaderSerializer = GameEventHeader.serializer()
    private val mapSerializer = MapSerializer(String.serializer(), TradableItem.serializer())

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(EventType.TRADABLE_PRICES) {
        element<GameEventHeader>("header")
        element<List<TradableItem>>("payload")
    }

    override fun deserialize(decoder: Decoder): TradablePricesEvent {
        val input = decoder.beginStructure(descriptor)
        var header: GameEventHeader? = null
        val items = mutableListOf<TradableItem>()

        loop@ while (true) {
            when (val index = input.decodeElementIndex(descriptor)) {
                0 -> header = input.decodeSerializableElement(descriptor, index, gameEventHeaderSerializer)
                1 -> {
                    val payloadDecoder = input.decodeSerializableElement(descriptor, index, mapSerializer)
                    items.addAll(payloadDecoder.values)
                }
                CompositeDecoder.DECODE_DONE -> break@loop
                else -> throw SerializationException("Unknown index $index")
            }
        }
        input.endStructure(descriptor)
        return TradablePricesEvent(header!!, ArrayList(items))
    }

    override fun serialize(encoder: Encoder, value: TradablePricesEvent) {
        val output = encoder.beginStructure(descriptor)
        output.encodeSerializableElement(descriptor, 0, gameEventHeaderSerializer, value.header)
        output.encodeSerializableElement(descriptor, 1, mapSerializer, value.payload.associateBy { it.name })
        output.endStructure(descriptor)
    }
}

