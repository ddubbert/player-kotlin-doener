package core.eventlistener

import io.github.classgraph.ClassGraph
import org.koin.core.component.KoinComponent
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

class EventHandlerRegistry : KoinComponent {

    private val handlers = mutableMapOf<KClass<out GameEvent>, EventHandler<*>>()

    init {
        scanAndRegisterEventHandlers()
    }

    private fun scanAndRegisterEventHandlers() {
        ClassGraph().enableAllInfo().scan().use { scanResult ->
            val handlerClasses = scanResult.getClassesImplementing(EventHandler::class.qualifiedName).loadClasses(
                EventHandler::class.java)
            for (handlerClass in handlerClasses) {
                val eventClass = handlerClass.kotlin.supertypes
                    .firstOrNull { it.classifier?.let { classifier -> classifier is KClass<*> && classifier.isSubclassOf(
                        EventHandler::class) } ?: false }
                    ?.arguments
                    ?.firstOrNull()
                    ?.type
                    ?.classifier as? KClass<out GameEvent>

                if (eventClass != null) {
                    val handlerInstance: EventHandler<*> = getKoin().get(handlerClass.kotlin) as EventHandler<*>
                    handlers[eventClass] = handlerInstance
                }
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <E : GameEvent> getEventHandlerForEvent(eventClass: KClass<out GameEvent>): EventHandler<E>? {
        return handlers[eventClass] as? EventHandler<E>
    }
}

