package player.application

import config.AppConfig
import core.restadapter.GameServiceRESTAdapter
import core.restadapter.PlayerRegistrationResponseDto
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import player.domain.PlayerRepository
import java.util.*

@ExtendWith(MockitoExtension::class)
class PlayerApplicationServiceTest {

    // Mocks
    @Mock
    lateinit var mockAppConfig: AppConfig

    @Mock
    lateinit var mockPlayerRepository: PlayerRepository

    @Mock
    lateinit var mockGameServiceRESTAdapter: GameServiceRESTAdapter

    @InjectMocks
    lateinit var playerApplicationService: PlayerApplicationService

    @BeforeEach
    fun setup() {
        Mockito.reset(mockAppConfig, mockPlayerRepository, mockGameServiceRESTAdapter)
    }

    @Test
    fun `fetchAndIfNeededCreatePlayer - player exists - should return player`() = runBlocking {
        // Arrange
        val expectedPlayerDto = PlayerRegistrationResponseDto(
            playerId = UUID.randomUUID(),
            name = "Hackschnitzel",
            email = "randommail@gmail.com",
            playerQueue = "player-Hackschnitzel",
            playerExchange = "player-Hackschnitzel"
        )
        Mockito.`when`(mockGameServiceRESTAdapter.sendGetRequestForPlayerDto()).thenReturn(expectedPlayerDto)

        // Act
        val result = playerApplicationService.fetchAndIfNeededCreatePlayer()

        // Assert
        Assertions.assertEquals(expectedPlayerDto, result)
    }





}
