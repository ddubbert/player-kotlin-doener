package core.eventlistener


import DungeonPlayerRuntimeException
import core.EventTestFactory
import core.EventTestFactory.Companion.createGameStatusEvent
import core.EventTestFactory.Companion.eventToJson
import core.eventlistener.concreteevents.game.GameStatusEvent
import core.eventlistener.concreteevents.game.RoundStatusEvent
import core.eventlistener.concreteevents.planet.PlanetDiscoveredEvent
import core.eventlistener.concreteevents.player.PlayerStatusEvent
import core.eventlistener.concreteevents.robot.RobotRegeneratedEvent
import core.eventlistener.concreteevents.robot.spawn.RobotSpawnedEvent
import core.eventlistener.concreteevents.trading.BankAccountClearedEvent
import core.eventlistener.concreteevents.trading.BankAccountInitializedEvent
import core.eventlistener.concreteevents.trading.BankAccountTransactionBookedEvent
import core.eventlistener.concreteevents.trading.TradablePricesEvent
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import java.util.*


class EventDeserializerServiceTest : KoinTest {

    private val deserializer: EventDeserializerService by inject<EventDeserializerService>()
    private val genericEventId = UUID.randomUUID()
    private val genericEventIdStr = genericEventId.toString()
    private val genericTransactionId = UUID.randomUUID()
    private val genericTransactionIdStr = genericTransactionId.toString()
    private val playerId = UUID.randomUUID()
    private val playerIdStr = playerId.toString()


    @BeforeEach
    fun setup() {
        startKoin {
            modules(
                module {
                    single { EventDeserializerService() }

                }
            )
        }
    }

    @AfterEach
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun `throws Exception when having unexpected type`() {
        val json = """
            {
                "header": {
                    "type": "Unknown Type",
                    "eventId": "$genericEventIdStr",
                    "kafka-topic": "unknown-topic",
                    "transactionId": "$genericTransactionIdStr",
                    "version": 1,
                    "playerId": "$playerIdStr",
                    "timestamp": "2023-10-04T12:34:56.789Z"
                },
                "payload": {
                    "gameId": "123e4567-e89b-12d3-a456-426614174002",
                    "gameworldId": "123e4567-e89b-12d3-a456-426614174003",
                    "status": "created"
                },
                "randomAddtionalKey": "RandomAdditionalValue"
            }
        """.trimIndent()


        assertThrows(DungeonPlayerRuntimeException::class.java) {
            deserializer.deserializeJsonStringToEvent(json)
        }
    }

    @Test
    fun `throws Exception while parsing when having an unknown key in event`() {
        val json = """
            {
                "header": {
                    "type": "${EventType.GAME_STATUS}",
                    "eventId": "$genericEventIdStr",
                    "kafka-topic": "unknown-topic",
                    "transactionId": "$genericTransactionIdStr",
                    "version": 1,
                    "playerId": "$playerIdStr",
                    "timestamp": "2023-10-04T12:34:56.789Z"
                },
                "payload": {
                    "gameId": "123e4567-e89b-12d3-a456-426614174002",
                    "gameworldId": "123e4567-e89b-12d3-a456-426614174003",
                    "status": "created"
                    "unknownKey": "unknownValue"

                },
                "type": "${EventType.GAME_STATUS}",
                
            }
        """.trimIndent()

        assertThrows(RuntimeException::class.java) {
            deserializer.deserializeJsonStringToEvent(json)
        }
    }

    @Test
    fun `should deserialize GameStatusEvent`() {
        val gameStatusEvent = createGameStatusEvent()
        val json = eventToJson(gameStatusEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is GameStatusEvent)
        val deserializedEvent = event as GameStatusEvent
        assertEquals(gameStatusEvent.payload.gameId, deserializedEvent.payload.gameId)
        assertEquals(gameStatusEvent.payload.gameworldId, deserializedEvent.payload.gameworldId)
        assertEquals(gameStatusEvent.payload.status, deserializedEvent.payload.status)
    }

    @Test
    fun `should deserialize BankAccountInitializedEvent`() {
        val bankAccountInitializedEvent = EventTestFactory.createBankAccountInitializedEvent()
        val json = EventTestFactory.eventToJson(bankAccountInitializedEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is BankAccountInitializedEvent)
        val deserializedEvent = event as BankAccountInitializedEvent
        assertEquals(bankAccountInitializedEvent.payload.playerId, deserializedEvent.payload.playerId)
        assertEquals(bankAccountInitializedEvent.payload.balance, deserializedEvent.payload.balance)
    }


    @Test
    fun `Deserialize TradablePricesEvent with custom Deserializer`() {
        val tradablePricesEvent = EventTestFactory.createTradablePricesEvent()
        val json = EventTestFactory.eventToJson(tradablePricesEvent)

        val deserializedEvent = deserializer.deserializeJsonStringToEvent(json) as TradablePricesEvent

        assertEquals(tradablePricesEvent, deserializedEvent)
        assertEquals(tradablePricesEvent.header, deserializedEvent.header)
        assertEquals(tradablePricesEvent.payload, deserializedEvent.payload)
    }

    @Test
    fun `should deserialize PlanetDiscoveredEvent`() {
        val planetDiscoveredEvent = EventTestFactory.createPlanetDiscoveredEvent()
        val json = EventTestFactory.eventToJson(planetDiscoveredEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is PlanetDiscoveredEvent)
        val deserializedEvent = event as PlanetDiscoveredEvent

        assertEquals(planetDiscoveredEvent.payload.planetId, deserializedEvent.payload.planetId)
        assertEquals(planetDiscoveredEvent.payload.neighbours, deserializedEvent.payload.neighbours)
        assertEquals(planetDiscoveredEvent.payload.resource, deserializedEvent.payload.resource)
        assertEquals(planetDiscoveredEvent.payload.movementDifficulty, deserializedEvent.payload.movementDifficulty)
    }

    @Test
    fun `should deserialize PlayerStatusEvent`() {
        val playerStatusEvent = EventTestFactory.createPlayerStatusEvent()
        val json = EventTestFactory.eventToJson(playerStatusEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is PlayerStatusEvent)
        val deserializedEvent = event as PlayerStatusEvent

        assertEquals(playerStatusEvent.payload.playerId, deserializedEvent.payload.playerId)
        assertEquals(playerStatusEvent.payload.gameId, deserializedEvent.payload.gameId)
        assertEquals(playerStatusEvent.payload.name, deserializedEvent.payload.name)

    }

    @Test
    fun `should deserialize RobotSpawnedEvent`() {
        val robotSpawnedEvent = EventTestFactory.createRobotSpawnedEvent()
        val json = EventTestFactory.eventToJson(robotSpawnedEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is RobotSpawnedEvent)
        val deserializedEvent = event as RobotSpawnedEvent

        assertEquals(robotSpawnedEvent.payload.playerId, deserializedEvent.payload.playerId)
        assertEquals(robotSpawnedEvent.payload.robotDto, deserializedEvent.payload.robotDto)

    }

    @Test
    fun `should deserialize BankAccountClearedEvent`() {
        val bankAccountClearedEvent = EventTestFactory.createBankAccountClearedEvent()
        val json = EventTestFactory.eventToJson(bankAccountClearedEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is BankAccountClearedEvent)
        val deserializedEvent = event as BankAccountClearedEvent

        assertEquals(bankAccountClearedEvent.payload.playerId, deserializedEvent.payload.playerId)
        assertEquals(bankAccountClearedEvent.payload.balance, deserializedEvent.payload.balance)
    }

    @Test
    fun `should deserialize BankAccountTransactionBookedEvent`() {
        val bankAccountTransactionBookedEvent = EventTestFactory.createBankAccountTransactionBookedEvent()
        val json = EventTestFactory.eventToJson(bankAccountTransactionBookedEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is BankAccountTransactionBookedEvent)
        val deserializedEvent = event as BankAccountTransactionBookedEvent

        assertEquals(bankAccountTransactionBookedEvent.payload.playerId, deserializedEvent.payload.playerId)
        assertEquals(bankAccountTransactionBookedEvent.payload.balance, deserializedEvent.payload.balance)
        assertEquals(bankAccountTransactionBookedEvent.payload.transactionAmount, deserializedEvent.payload.transactionAmount)
    }

    @Test
    fun `should deserialize RobotRegeneratedEvent`() {
        val robotRegeneratedEvent = EventTestFactory.createRobotRegeneratedEvent()
        val json = EventTestFactory.eventToJson(robotRegeneratedEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is RobotRegeneratedEvent)
        val deserializedEvent = event as RobotRegeneratedEvent

        assertEquals(robotRegeneratedEvent.payload.robotId, deserializedEvent.payload.robotId)
        assertEquals(robotRegeneratedEvent.payload.availableEnergy, deserializedEvent.payload.availableEnergy)
    }


    @Test
    fun `should deserialize RoundStatusEvent`() {
        val roundStatusEvent = EventTestFactory.createRoundStatusEvent()
        val json = EventTestFactory.eventToJson(roundStatusEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is RoundStatusEvent)
        val deserializedEvent = event as RoundStatusEvent
        assertEquals(roundStatusEvent.payload, deserializedEvent.payload)
    }



    @Test
    fun `should deserialize TradablePricesEvent`() {
        val tradablePricesEvent = EventTestFactory.createTradablePricesEvent()
        val json = EventTestFactory.eventToJson(tradablePricesEvent)

        val event = deserializer.deserializeJsonStringToEvent(json)

        assertTrue(event is TradablePricesEvent)
        val deserializedEvent = event as TradablePricesEvent
        assertEquals(tradablePricesEvent.payload, deserializedEvent.payload)
    }


}