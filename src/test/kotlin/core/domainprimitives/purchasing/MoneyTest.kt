package core.domainprimitives.purchasing

import core.domainprimitives.DomainPrimitiveException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class MoneyTest {

    private lateinit var m27_1: Money
    private lateinit var m27_2: Money
    private lateinit var m28: Money
    private lateinit var m0: Money
    private lateinit var m3: Money
    private lateinit var m3b: Money
    private lateinit var m17: Money
    private lateinit var m20: Money

    @BeforeEach
    fun setUp() {
        m27_1 = Money.from(BigDecimal.valueOf(27))
        m27_2 = Money.from(BigDecimal.valueOf(27))
        m28 = Money.from(BigDecimal.valueOf(28))
        m0 = Money.from(BigDecimal.ZERO)
        m3 = Money.from(BigDecimal.valueOf(3))
        m3b = Money.from(BigDecimal.valueOf(3))
        m17 = Money.from(BigDecimal.valueOf(17))
        m20 = Money.from(BigDecimal.valueOf(20))
    }

    @Test
    fun testTwoMoneyEqualAndUnequal() {
        assertEquals(m27_1, m27_2)
        assertNotEquals(m27_1, m28)
    }

    @Test
    fun testValidation() {
        assertThrows(DomainPrimitiveException::class.java) {
            Money.from(BigDecimal.valueOf(-1))
        }
    }

    @Test
    fun testGreater() {
        assertTrue(m3.greaterEqualThen(m3b))
        assertFalse(m3.greaterThan(m3))
        assertTrue(m3.greaterEqualThen(m0))
        assertFalse(m0.greaterThan(m3))
    }

    @Test
    fun testDecrease() {
        assertEquals(m3.decreaseBy(m0), m3b)
        assertEquals(m3.decreaseBy(m3b), m0)
        assertEquals(m20.decreaseBy(m3), m17)
        assertThrows(DomainPrimitiveException::class.java) {
            m3.decreaseBy(m20)
        }
    }

    @Test
    fun testIncrease() {
        assertEquals(m3.increaseBy(m0), m3b)
        assertEquals(m17.increaseBy(m3), m20)
    }

    @Test
    fun testZero() {
        val zero = Money.zero()
        assertEquals(zero, m0)
    }

    @Test
    fun testCanBuyThatManyFor() {
        assertEquals(BigDecimal.valueOf(9), m27_1.canBuyThatManyFor(m3))
        assertThrows(DomainPrimitiveException::class.java) {
            m27_1.canBuyThatManyFor(m0)
        }
    }
}
