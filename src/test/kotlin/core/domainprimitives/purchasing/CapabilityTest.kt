import core.domainprimitives.DomainPrimitiveException
import core.domainprimitives.purchasing.Capability
import core.domainprimitives.purchasing.CapabilityType
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows


class CapabilityTest {
    private lateinit var d0: Capability
    private lateinit var d1: Capability
    private lateinit var dmax: Capability
    private lateinit var h1: Capability

    @BeforeEach
    fun setUp() {
        d0 = Capability.baseForType(CapabilityType.HEALTH)
        d1 = Capability.forTypeAndLevel(CapabilityType.HEALTH, 1)
        dmax = Capability.forTypeAndLevel(CapabilityType.HEALTH, Capability.MAX_LEVEL)
        h1 = Capability.forTypeAndLevel(CapabilityType.DAMAGE, 1)
    }

    @Test
    fun testValidation() {
        assertThrows(DomainPrimitiveException::class.java) {
            Capability.baseForType(
                null
            )
        }
        assertThrows(
            DomainPrimitiveException::class.java
        ) { Capability.forTypeAndLevel(CapabilityType.DAMAGE, null) }
        assertThrows(
            DomainPrimitiveException::class.java
        ) {
            Capability.forTypeAndLevel(
                CapabilityType.DAMAGE,
                Capability.MIN_LEVEL - 1
            )
        }
        assertThrows(
            DomainPrimitiveException::class.java
        ) {
            Capability.forTypeAndLevel(
                CapabilityType.DAMAGE,
                Capability.MAX_LEVEL + 1
            )
        }
    }

    @Test
    fun testNextLevel() {
        // given
        // when
        val healthCapabilityLevel1 = d0.nextLevel()

        // then
        assertEquals(d1, healthCapabilityLevel1)
        assertThrows<DomainPrimitiveException> {
            dmax.nextLevel()
        }
    }

    @Test
    fun testEquals() {
        // given
        // when
        val newD1 = Capability.forTypeAndLevel(CapabilityType.HEALTH, 1)

        // then
        assertEquals(d1, newD1)
        assertNotEquals(d1, h1)
        assertNotEquals(d0, d1)
    }

    @Test
    fun testToStringForCommand() {
        // given
        val cer3 = Capability.forTypeAndLevel(CapabilityType.ENERGY_REGEN, 3)
        val ch5 = Capability.forTypeAndLevel(CapabilityType.HEALTH, 5)

        // when
        // then
        assertEquals("ENERGY_REGEN_3", cer3.toStringForCommand())
        assertEquals("HEALTH_5", ch5.toStringForCommand())
    }


    @Test
    fun `should create base capability for given type`() {
        val damageCapability = Capability.baseForType(CapabilityType.DAMAGE)
        assertEquals("DA-0", damageCapability.toString())

        val healthCapability = Capability.baseForType(CapabilityType.HEALTH)
        assertEquals("H-0", healthCapability.toString())
    }

    @Test
    fun `should create capability for given type and level`() {
        val energyCapability = Capability.forTypeAndLevel(CapabilityType.MAX_ENERGY, 3)
        assertEquals("ME-3", energyCapability.toString())

        val miningCapability = Capability.forTypeAndLevel(CapabilityType.MINING, 5)
        assertEquals("MI-5", miningCapability.toString())
    }

    @Test
    fun `should throw exception for invalid level`() {
        assertThrows<DomainPrimitiveException> {
            Capability.forTypeAndLevel(CapabilityType.STORAGE, -1)
        }

        assertThrows<DomainPrimitiveException> {
            Capability.forTypeAndLevel(CapabilityType.STORAGE, 6)
        }
    }

    @Test
    fun `should return all base capabilities`() {
        val baseCapabilities = Capability.allBaseCapabilities()
        assertTrue(baseCapabilities.contains(Capability.baseForType(CapabilityType.DAMAGE)))
        assertTrue(baseCapabilities.contains(Capability.baseForType(CapabilityType.HEALTH)))
        // ... und so weiter für andere Typen
    }

    @Test
    fun `should return next level capability`() {
        val miningCapability = Capability.forTypeAndLevel(CapabilityType.MINING_SPEED, 2)
        assertEquals("MS-3", miningCapability.nextLevel().toString())
    }

    @Test
    fun `should throw DomainPrimitiveException for next level if at max level`() {
        val storageCapability = Capability.forTypeAndLevel(CapabilityType.STORAGE, 5)
        assertThrows<DomainPrimitiveException> { storageCapability.nextLevel() }
    }

    @Test
    fun `should check for minimum and maximum level`() {
        val minimumLevelCapability = Capability.baseForType(CapabilityType.ENERGY_REGEN)
        assertTrue(minimumLevelCapability.isMinimumLevel())
        assertFalse(minimumLevelCapability.isMaximumLevel())

        val maximumLevelCapability = Capability.forTypeAndLevel(CapabilityType.ENERGY_REGEN, 5)
        assertFalse(maximumLevelCapability.isMinimumLevel())
        assertTrue(maximumLevelCapability.isMaximumLevel())
    }

}
