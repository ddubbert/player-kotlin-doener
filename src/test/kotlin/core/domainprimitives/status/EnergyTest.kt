package core.domainprimitives.status

import core.domainprimitives.DomainPrimitiveException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class EnergyTest {

    private lateinit var e0: Energy
    private lateinit var e3: Energy
    private lateinit var e3b: Energy
    private lateinit var e17: Energy
    private lateinit var e20: Energy

    @BeforeEach
    fun setUp() {
        e0 = Energy.from(0)
        e3 = Energy.from(3)
        e3b = Energy.from(3)
        e17 = Energy.from(17)
        e20 = Energy.from(20)
    }

    @Test
    fun testEqualAndUnequal() {
        assertEquals(e3, e3b)
        assertNotEquals(e3, e20)
        assertEquals(Energy.zero(), e0)
    }

    @Test
    fun testValidation() {
        assertThrows(DomainPrimitiveException::class.java) {
            Energy.from(-1)
        }
    }

    @Test
    fun testGreater() {
        assertTrue(e3.greaterEqualThen(e3b))
        assertFalse(e3.greaterThan(e3))
        assertTrue(e3.greaterEqualThen(e0))
        assertFalse(e0.greaterThan(e3))
    }

    @Test
    fun testDecrease() {
        assertEquals(e3.decreaseBy(e0), e3b)
        assertEquals(e3.decreaseBy(e3b), e0)
        assertEquals(e20.decreaseBy(e3), e17)
        assertThrows(DomainPrimitiveException::class.java) {
            e3.decreaseBy(e17)
        }
    }

    @Test
    fun testIncrease() {
        assertEquals(e3.increaseBy(e0), e3b)
        assertEquals(e17.increaseBy(e3), e20)
    }

    @Test
    fun testLowerThanPercentage() {
        assertTrue(e3.lowerThanPercentage(16, e20))
        assertFalse(e3.lowerThanPercentage(15, e20))
        assertThrows(DomainPrimitiveException::class.java) {
            e3.lowerThanPercentage(-1, e20)
        }
        assertThrows(DomainPrimitiveException::class.java) {
            e3.lowerThanPercentage(101, e20)
        }
    }
}