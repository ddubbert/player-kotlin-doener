package core.domainprimitives.location

import core.domainprimitives.DomainPrimitiveException
import core.domainprimitives.location.MinableResource.Companion.fromTypeAndAmount
import org.junit.Assert.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class MinableResourceTest {
    private lateinit var gold12_1: MinableResource
    private lateinit var gold12_2: MinableResource
    private lateinit var platin12: MinableResource

    @BeforeEach
    fun setUp() {
        gold12_1 = fromTypeAndAmount(MinableResourceType.GOLD, 12)
        gold12_2 = fromTypeAndAmount(MinableResourceType.GOLD, 12)
        platin12 = fromTypeAndAmount(MinableResourceType.PLATIN, 12)
    }
    @Test
    fun testEqualAndUnequal() {
        assertEquals(gold12_1, gold12_2)
        assertNotEquals(gold12_1, platin12)
    }

    @Test
    fun testValidation(){
        assertThrows(
            DomainPrimitiveException::class.java
        ) { fromTypeAndAmount(null, 12) }
        assertThrows(
            DomainPrimitiveException::class.java
        ) { fromTypeAndAmount(MinableResourceType.GOLD, 0) }
        assertThrows(
            DomainPrimitiveException::class.java
        ) { fromTypeAndAmount(MinableResourceType.GOLD, -1) }

    }
}