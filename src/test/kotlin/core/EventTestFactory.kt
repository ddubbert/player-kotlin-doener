package core

import core.domainprimitives.location.MinableResourceType
import core.domainprimitives.purchasing.*
import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.concreteevents.game.*
import core.eventlistener.concreteevents.planet.PlanetDiscoveredEvent
import core.eventlistener.concreteevents.planet.PlanetDiscoveredPayload
import core.eventlistener.concreteevents.planet.PlanetResourceDto
import core.eventlistener.concreteevents.player.PlayerStatusEvent
import core.eventlistener.concreteevents.player.PlayerStatusPayload
import core.eventlistener.concreteevents.robot.RobotRegeneratedEvent
import core.eventlistener.concreteevents.robot.RobotRegeneratedPayload
import core.eventlistener.concreteevents.robot.spawn.*
import core.eventlistener.concreteevents.trading.*
import core.eventlistener.serializers.GameEventSerializer
import game.domain.GameStatus
import io.github.classgraph.ClassGraph
import kotlinx.datetime.toInstant
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.serializer
import java.math.BigDecimal
import java.util.*
import kotlin.reflect.KClass

/**
 * This is a factory for creating events for testing purposes.
 * The JSON construction uses the event documentation as input
 */
class EventTestFactory {

    companion object {
        private val jsonMapper: Json by lazy {
            initJsonMapper()
        }



        fun eventToJson(event: GameEvent): String {
            // Assumes GameEvent is the common super type of all event classes
            val json = jsonMapper.encodeToString(GameEventSerializer, event)
            return json
        }

        fun createGameStatusEvent(): GameStatusEvent {
            return GameStatusEvent(
                header = createGameEventHeader(EventType.GAME_STATUS),
                payload = createGameStatusPayload()
            )
        }

        fun createImpreciseTimingPredictions(): ImpreciseTimingPredictions {
            return ImpreciseTimingPredictions(
                roundStart = "2023-01-01T12:00:00Z",
                commandInputEnd = "2023-01-01T12:10:00Z",
                roundEnd = "2023-01-01T12:15:00Z"
            )
        }

        fun createImpreciseTimings(): ImpreciseTimings {
            return ImpreciseTimings(
                roundStarted = "2023-01-01T12:00:00Z",
                commandInputEnded = "2023-01-01T12:10:00Z",
                roundEnded = "2023-01-01T12:15:00Z"
            )
        }

        private fun createGameEventHeader(type: String): GameEventHeader {
            // Assume the GameEventHeader class has a constructor that takes a UUID and a timestamp
            return GameEventHeader(
                type = type,
                eventId = UUID.randomUUID(),
                timestamp = "2023-10-04T12:34:56.789Z".toInstant(),
                kafkaTopic = "unknown-topic",
                transactionId = UUID.randomUUID(),
                version = 1,
                playerId = "public"
            )
        }

        private fun createGameStatusPayload(): GameStatusPayload {
            return GameStatusPayload(
                gameId = UUID.randomUUID(),
                gameworldId = UUID.randomUUID(),
                status = GameStatus.STARTED  // Assume there's an ACTIVE status in GameStatus enum
            )
        }


        fun createRoundStatusEvent(): RoundStatusEvent {
            return RoundStatusEvent(
                header = createGameEventHeader(EventType.ROUND_STATUS),
                payload = createRoundStatusPayload()
            )
        }

        private fun createRoundStatusPayload(): RoundStatusPayload {
            return RoundStatusPayload(
                gameId = UUID.randomUUID(),
                roundId = UUID.randomUUID(),
                roundNumber = 1,
                roundStatus = RoundStatusType.STARTED,
                impreciseTimings = createImpreciseTimings(),
                impreciseTimingPredictions = createImpreciseTimingPredictions()
            )
        }

        fun createRoundStatusType(): RoundStatusType {
            return RoundStatusType.entries.toTypedArray().random()
        }

        fun createPlanetDiscoveredEvent(): PlanetDiscoveredEvent {
            return PlanetDiscoveredEvent(
                header = createGameEventHeader(EventType.PLANET_DISCOVERED),
                payload = createPlanetDiscoveredPayload()
            )
        }

        private fun createPlanetDiscoveredPayload(): PlanetDiscoveredPayload {
            return PlanetDiscoveredPayload(
                planetId = UUID.randomUUID(),
                movementDifficulty = 1,
                neighbours = arrayListOf(UUID.randomUUID(), UUID.randomUUID()),
                resource = createPlanetResourceDto()
            )
        }

        private fun createPlanetResourceDto(): PlanetResourceDto {
            return PlanetResourceDto(
                resourceType = MinableResourceType.COAL,  // Assume MinableResourceType has a COAL enum value
                maxAmount = 100,
                currentAmount = 50
            )
        }

        fun createPlayerStatusEvent(): PlayerStatusEvent {
            return PlayerStatusEvent(
                header = createGameEventHeader(EventType.PLAYER_STATUS),
                payload = createPlayerStatusPayload()
            )
        }

        private fun createPlayerStatusPayload(): PlayerStatusPayload {
            return PlayerStatusPayload(
                playerId = UUID.randomUUID(),
                gameId = UUID.randomUUID(),
                name = "Test Player"
            )
        }

        fun createRobotSpawnedEvent(): RobotSpawnedEvent {
            return RobotSpawnedEvent(
                header = createGameEventHeader(EventType.ROBOT_SPAWNED),
                payload = createRobotSpawnedPayload()
            )
        }

        private fun createRobotSpawnedPayload(): RobotSpawnedPayload {
            return RobotSpawnedPayload(
                playerId = UUID.randomUUID(),
                robotDto = createRobotDto()
            )
        }

        private fun createRobotDto(): RobotDto {
            return RobotDto(
                player = UUID.randomUUID(),
                planet = createRobotPlanetDto(),
                id = UUID.randomUUID(),
                alive = true,
                inventory = createRobotInventoryDto(),
                health = 100,
                energy = 100,
                miningSpeed = 10,
                maxHealth = 100,
                maxEnergy = 100,
                energyRegen = 10,
                attackDamage = 10
            )
        }

        private fun createRobotPlanetDto(): RobotPlanetDto {
            return RobotPlanetDto(
                planetId = UUID.randomUUID(),
                gameWorldId = UUID.randomUUID(),
                movementDifficulty = 1,
                resourceType = "COAL"  // Assume there's a "COAL" resource type
            )
        }

        private fun createRobotInventoryDto(): RobotInventoryDto {
            return RobotInventoryDto(
                storageLevel = 1,
                usedStorage = 0,
                resources = createRobotInventoryResourcesDto(),
                full = false,
                maxStorage = 100
            )
        }

        private fun createRobotInventoryResourcesDto(): RobotInventoryResourcesDto {
            return RobotInventoryResourcesDto(
                coal = 10,
                iron = 10,
                gem = 10,
                gold = 10,
                platin = 10
            )
        }


        fun createBankAccountClearedEvent(): BankAccountClearedEvent {
            return BankAccountClearedEvent(
                header = createGameEventHeader(EventType.BANK_ACCOUNT_CLEARED),
                payload = createBankAccountClearedPayload()
            )
        }

        private fun createBankAccountClearedPayload(): BankAccountClearedPayload {
            return BankAccountClearedPayload(
                playerId = UUID.randomUUID(),
                balance = BigDecimal.ZERO
            )
        }

        fun createBankAccountInitializedEvent(): BankAccountInitializedEvent {
            return BankAccountInitializedEvent(
                header = createGameEventHeader(EventType.BANK_ACCOUNT_INITIALIZED),
                payload = createBankAccountInitializedPayload()
            )
        }

        private fun createBankAccountInitializedPayload(): BankAccountInitializedPayload {
            return BankAccountInitializedPayload(
                playerId = UUID.randomUUID(),
                balance = BigDecimal.valueOf(100)
            )
        }

        fun createBankAccountTransactionBookedEvent(): BankAccountTransactionBookedEvent {
            return BankAccountTransactionBookedEvent(
                header = createGameEventHeader(EventType.BANK_ACCOUNT_TRANSACTION_BOOKED),
                payload = createBankAccountTransactionBookedPayload()
            )
        }

        private fun createBankAccountTransactionBookedPayload(): BankAccountTransactionBookedPayload {
            return BankAccountTransactionBookedPayload(
                playerId = UUID.randomUUID(),
                balance = 100.0,
                transactionAmount = 10
            )
        }

        fun createTradablePricesEvent(): TradablePricesEvent {
            return TradablePricesEvent(
                header = createGameEventHeader(EventType.TRADABLE_PRICES),
                payload = listOf(
                    TradableItem(name = ItemTypeDto.ROBOT.name, price = 100, type = TradableType.ITEM),
                    TradableItem(
                        name = Capability.forTypeAndLevel(CapabilityType.DAMAGE, 1).toStringForCommand(),
                        price = 200,
                        type = TradableType.UPGRADE
                    ),
                    TradableItem(name = MinableResourceType.COAL.name, price = 100, type = TradableType.RESOURCE),
                    TradableItem(name = ItemTypeDto.HEALTH_RESTORE.name, price = 100, type = TradableType.RESTORATION),
                )
            )
        }

        fun createRobotRegeneratedEvent(): RobotRegeneratedEvent {
            return RobotRegeneratedEvent(
                header = createGameEventHeader(EventType.ROBOT_REGENERATED),
                payload = createRobotRegeneratedPayload()
            )
        }

        private fun createRobotRegeneratedPayload(): RobotRegeneratedPayload {
            return RobotRegeneratedPayload(
                robotId = UUID.randomUUID(),
                availableEnergy = 100
            )
        }

        private fun initJsonMapper() :Json {
            val gameEventSubclasses = ClassGraph()
                .enableClassInfo()
                .acceptPackages("core.eventlistener.concreteevents") // We are using reflection to find all subclasses of GameEvent
                .scan()
                .getSubclasses(GameEvent::class.qualifiedName)

            val module = SerializersModule {
                polymorphic(GameEvent::class) {
                    gameEventSubclasses.forEach { classInfo ->
                        val className = classInfo.name

                        @Suppress("UNCHECKED_CAST")
                        val kClass = Class.forName(className).kotlin as KClass<GameEvent>

                        @OptIn(InternalSerializationApi::class)
                        val kSerializer = kClass.serializer()
                        subclass(kClass, kSerializer)
                    }
                }
            }

            return Json{
                serializersModule = module
                coerceInputValues = true
                ignoreUnknownKeys = false
            }
        }


    }
}